/*
 * Piotr Szczepański 226530 RTOS PART1/Task1&2
 */


#include <thread>
#include <iostream>
#include <cstdint>
#include <mutex>
#include <vector>
#include <memory>

class StorageBase
{
public:
	explicit StorageBase(const uint32_t val): globalVariable_(val){};

	virtual void addValue(const uint32_t val) = 0;
	uint32_t get(){return globalVariable_;};

protected:
	uint32_t globalVariable_;
};

class StorageMutex final : public StorageBase
{
public:
	explicit StorageMutex(const uint32_t val): StorageBase(val){};

	void addValue(const uint32_t val) override
	{
		const std::lock_guard<std::mutex> lock(mutex_);
		globalVariable_ += val;
	}
private:
	std::mutex mutex_;
};


class Storage final : public StorageBase
{
public:
	explicit Storage(const uint32_t val): StorageBase(val){};

	void addValue(const uint32_t val) override
	{
		globalVariable_ += val;
	}
};

int main()
{
	const uint32_t toAdd{1};
	const uint32_t start{0};

	std::vector<std::unique_ptr<StorageBase>> storages;
	storages.emplace_back(new Storage(start));
	storages.emplace_back(new StorageMutex(start));

	for (auto & storage : storages)
	{
		auto increment = [&storage, toAdd](){for (uint32_t i = 0; i < 10000; ++i)
			{storage->addValue(toAdd);}};

		std::vector<std::thread> threads;

		for (uint32_t i = 0; i < 16; ++i)
		{
			threads.emplace_back(increment);
		}

		for (auto & thread : threads)
		{
			if(thread.joinable())
			{
				thread.join();
			}
		}
	}

	std::clog << "Without Mutex: " << storages.at(0)->get() << '\n';
	std::clog << "With Mutex   : " << storages.at(1)->get() << '\n';
}
