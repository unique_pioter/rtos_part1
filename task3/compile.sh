#!/bin/bash

RED='\033[0;31m'
NC='\033[0m' # No Color
GREEN='\033[0;32m'
BLUE='\033[0;34m'

mkdir -p ./build

cd ./build
cmake ..
make all 
./test/task3_test --gtest_color=yes 

if [[ $? -eq 0 ]]; then
	echo -e "${BLUE}All test passed, running target binary ! ${NC}"
	./src/task3_run
else
	echo -e "${RED}SOME TEST FAILED, ABORTING ! ${NC}"
fi
