#pragma once

#include <thread>
#include <iostream>
#include <cstdint>
#include <mutex>
#include <vector>
#include <memory>
#include <queue>

#include "Interfaces.hpp"

class DataStorage : public IReadable, public IWritable, IServer
{
public:
	DataStorage();
	~DataStorage();

	uint8_t read() override;
	bool write(const uint8_t bite) override;

	void readFinished() override;
	void writeFinished() override;

private:
	std::vector<uint8_t> data_;
	const uint8_t maxSize_;

	StorageState currentState_;
};