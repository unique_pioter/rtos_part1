#pragma once

#include <cstdint>

class IReadable
{
	virtual uint8_t read() = 0;
	virtual void readFinished() = 0;
};

class IWritable
{
	virtual bool write(uint8_t) = 0;
	virtual void writeFinished() = 0;
};

enum class StorageState
{
	RX,
	TX,
	IDLE,
	FULL
};

class IStates
{
public:
	virtual void stateChanged(const StorageState state) const = 0;
};

class IServer
{
protected:
	void addClient(std::shared_ptr<IStates> client)
	{
		clients_.push_back(client);
	}

	void informClients(const StorageState state)
	{
		for (const auto & client : clients_)
		{
			client->stateChanged(state);
		}
	}

	std::vector<std::shared_ptr<IStates>> clients_;
};

// class Notifier
// {
// public:
// 	explicit Notifier(IServer server, const StorageState state): server_(server)
// 	{
// 		server_.informClients(state);
// 	}

// 	~Notifier(const StorageState state)
// 	{
// 		server_.informClients(state);
// 	}

// 	std::shared_ptr<IServer> server_;
// };