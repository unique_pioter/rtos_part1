#include "DataStorage.hpp"

DataStorage::DataStorage(): maxSize_(20), currentState_(StorageState::IDLE)
{}

uint8_t DataStorage::read()
{
	currentState_ = StorageState::RX;
	// Notifier notifier(this, currentState_);
	informClients(currentState_);
	auto bite = data_.back();
	data_.pop_back();
	return bite;
}

bool DataStorage::write(const uint8_t bite)
{
	data_.push_back(bite);
}

void DataStorage::readFinished()
{
	informClients(currentState_);
}

void DataStorage::writeFinished()
{
	if (data_.size() == maxSize_)
	{
		currentState_ = StorageState::FULL; 
	}
	else 
	{
		currentState_ == StorageState::IDLE;
	}

	informClients(currentState_);
}
