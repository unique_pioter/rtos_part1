#pragma once

#include <memory>
#include <vector>

#include "Intefaces.hpp"
#include "Workers.hpp"

class Dispatcher : public IStates
{
public:
	void stateChanged(const StorageState state) const override;

private:
	std::vector<std::shared_ptr<Reader>> readers;
	std::vector<std::shared_ptr<Writer>> writers;
};