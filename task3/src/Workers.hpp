#pragma once

#include <thread>
#include <iostream>
#include <cstdint>
#include <mutex>
#include <vector>
#include <memory>

#include "Interfaces.hpp"

class Reader
{
public:
	Reader();

	uint8_t readData();

	bool isDataValid();

private:
	std::vector<uint8_t> data_;
	const uint8_t dataType;

	std::shared_ptr<IReadable> storageHandler_;
};


class Writer
{
public:
	Writer();
	// Writer(const std::fuction<void(void)>& );

	bool isAllDataTransfered();

private:
	std::vector<uint8_t> dataToTransfer_;
	std::shared_ptr<IWritable> storageHandler_;

	// some handler to datastorage
};
