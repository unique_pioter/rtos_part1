#include "gtest/gtest.h"
#include "gtest/gmock.h"

#include "Interfaces.hpp"
#include "Workers.hpp"
#include "DataStorage.hpp"

class CilentMock : public IStates
{
public:
	MOCK_METHOD1(void, stateChanged, StorageState state, (override));
}

class TestBase : public ::testing::Test
{};

TEST_F(TestBase, Signals)
{
	DataStorage storage;
	std::shared_ptr<IStates> client(new CilentMock);
	storage.addClient(client);
	storage.write(1);
	storage.write(2);

	storage.read();
	EXPECT_CALL(&client, stateChanged(StorageState::RX));
	storage.readFinished();
	EXPECT_CALL(&client, stateChanged(StorageState::IDLE));
}

TEST_F(TestBase, OneReaderOneWriter)
{

}


int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}